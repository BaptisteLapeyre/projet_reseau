import socket 
import sys
import datetime
import locale
import threading
import re 
import json
import logging
from logging.handlers import RotatingFileHandler

TAILLE_TAMPON = 256

class Serv(object):
    def __init__(self, ip, port):

        # creation et configuration des logs
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
        file_handler = RotatingFileHandler('servLog.log', 'a', 1000000, 1)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        self.clients = {} # dictionnaire faisant corrspondre a tout les client connecté leur socket
        self.acceptedPM = {}    # dictionnaire faisant correspondre a tout les client connecté une liste des client qui on accepté ses message privé
        self.askedPM = {}   # dictionnaire de la liste des personnes ayant demandé une conversation privé avec un client
        self.askedFile = {}# dictionnaire de la liste des personnes ayant demandé un envoie de fichier a un client
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #creation du socket
        self.sock.bind((ip, port))
        self.sock.listen(5)
        print("Serveur en attente sur le port " + str(port), file=sys.stderr)
        self.logger.info("Server waiting on the port " + str(port))

        while True: # boucle jusqua l'arret du serveur avec ctrl+c
            try:
                # creation et lancement d'un nouveau thread pour chaque client qui fait un socket.connect()
                (clientsocket, address) = self.sock.accept()
                self.logger.info("create socket for " + address[0])
                thread = threading.Thread(target = self.thread, args = (clientsocket, address[0]))
                thread.start()
            except KeyboardInterrupt: break
        #arret du serveur
        print("Arrêt du serveur", file=sys.stderr) 
        self.logger.info("server shutting down")
        self.sock.close() #fermeture du socket en attente des cnnect
        for s in self.clients.values(): #fermeture du tout les socket deja connecté
            try:
                s.close()
            except Exception:
                pass
        self.logger.info("server stopped")

    # test de la validité du pseudo d'un client
    def nameValid(self, name):
        return (len(name.split(" ")) != 1 or len(name) > 15 or not re.match('[a-zA-Z0-9_]',name))
    
    # envoie une notification a tout les clients connecté 
    def SendNotif(self, name, notif):
        for sockClient in self.clients.values():
            try:
                sockClient.send(("NOTIFYALL " + name + " " + notif + "\n").encode())
            except Exception: # si le socket.send ne marche pas, le client est surement deconnecté
                pass #la deconncetion du client et la fermeture du socket de cet utilisateur sera geré dans son thread au moment du recv

    def thread(self, clientsocket, ip, pseudo = None): #chaque nouveau client qui fait un socket.connect arrive dans cette fonction avec sont propre thread
        afk = False
        mes = ""
        while True: # le thread va boucler tant que le client n'est pas deconnecté
            while "\n" not in mes: # le thread fait des recv pour recupperer le requetes du client jusq'a avoir un \n qui signifie que la commande est fini
                try:
                    print("0")
                    mes += clientsocket.recv(2048).decode() # le recv est socké dans le buffer mes
                    if mes == "": # si le recv passe sans rien renvoyer le client est surement connecté
                        mes = "QUIT\n" # on ecris la commande QUIT en dur pour executer le code qui deconnectera proprement le client
                except Exception: # si le recv echoue, le client a surmement fermé le socket
                    if pseudo: # si le client est connecté
                        mes = "QUIT\n" # on ecris la commande QUIT en dur pour executer le code qui deconnectera proprement le client
                    else:
                        clientsocket.close() # on ferme le socket 
                        break
            
            mse = mes.replace('\r','') # suite a des test selon le client utilisé le recv renvoi parfois des \r en plus des \n
            message, mes = mes.split("\n",1) # on recupere dans "message" et on enleve de "mes" la premiere commande contenue dans "mes"
            self.logger.info("from " + ip + ": " + message)
            # on recupere dans "commande" la premiere parti de "message" qui est soit le nom de la commande soit le numero du retour et on met le reste dans "arg"
            if " " in message: 
                commande, arg = message.split(" ",1)
            else:
                commande = message
                arg = None
            if re.match('[0-9]',commande): # si la commende recu du client et un retour on ne s'occupe que des cas ou ce sont des retour de askpm et sendfile
                if arg.count(' ') >= 1:
                    _, askfrom = arg.split(" ",1) # si il y a au moins 2 parametres on recupere le deuxieme qui doit etre le pseudo du demandeur dans les cas qui nous interesse
                    if askfrom in self.askedPM[pseudo]: # on test ce pseudo pour savoir si il nous a bien envoyé une demande de message privé (elimine aussi les cas ou il s'ajit d'un retour pour une commande qui nous interesse pas)
                        self.askedPM[pseudo].remove(askfrom) # peut importe si la demande est accepté ou refusé on la retire de la liste
                        if commande == "110": # si elle est accepté on transmer l'acceptation au demandeur
                            self.clients[askfrom].send( ("110 ACCEPT " + pseudo + "\n").encode())
                            # ajout des deux interlicuteur la la liste des personne acceptés de l'autre
                            self.acceptedPM[pseudo].append(askfrom)
                            self.acceptedPM[askfrom].append(pseudo)
                        elif commande == "120": # la demande est refusé, on transmet le refus
                            self.clients[askfrom].send( ("120 REFUSE " + pseudo + "\n").encode())
                    if askfrom.count(' ') == 2: #si il y a encore 3 arguments dans "askfrom" on est surement dans le cas de lacceptation de l'envoie d'un fichier
                        askfrom, connection = askfrom.split(" ",1) # on separe donc le pseudo du demandeur de l'ip et du port a utilisé pour l'envoi
                    if askfrom in self.askedFile[pseudo]: # on test le pseudo pour savoir si il nous a bien envoyé une demande de message privé 
                        self.askedPM[pseudo].remove(askfrom) # peut importe si la demande est accepté ou refusé on la retire de la liste
                        if commande == "111": # si elle est accepté on transmer l'acceptation au demandeur avec l'ip et le port sur lequel envoyer le fichier
                            self.clients[askfrom].send( ("111 ACCEPT " + pseudo + connection +"\n").encode())
                        elif commande == "120": # la demande est refusé, on transmet le refus
                            self.clients[askfrom].send( ("120 REFUSE " + pseudo + "\n").encode())
                continue

            if commande == "CONNECT": # le client demande a ce connecter avec un pseudo
                if pseudo: # si il y a deja un pseudo, le cleint est deja connecté
                    reponse = "300 ERR_ALREADY_CONNECTED"
                elif self.nameValid(arg): # le pseudo donné ne satisfait pas les regles decritent dans la rfc
                    reponse = "400 ERR_INVALID_NICK_NAME"
                elif arg in self.clients.keys(): # un autre utilisateur a deja ce pseudo
                    reponse = "410 ERR_PSEUDO_ALREADY_USE"
                else: # le client ce connecte sous le nom passé en parametre
                    pseudo = arg
                    self.clients[arg] = clientsocket # ajoute de sont pseudo et de sont socket au dictionnaire des client connecté
                    self.SendNotif(pseudo, "connected") # nitification a tout les clients connectés que le client "pseudo" est arrivé
                    self.acceptedPM[pseudo] = [] # creation de la liste des clients ayant accepté les massages privé du client qui correspond a ce thread
                    self.askedPM[pseudo] = []
                    self.askedFile[pseudo] = []
                    reponse = "100 SUCCESS" # reponse au client que la connection s'est bien passé
            elif pseudo: # si le client est connecté (toute les commande suivante necessite que le client soit connecté)
                if commande == "QUIT": # le client veut partir du chat
                    del self.clients[pseudo] # supression du client et de sont socket du dictionnaire des connecté
                    for c in self.acceptedPM[pseudo]: # suppresion du client dans la liste des cleints l'ayant accepter en message privé
                        self.acceptedPM[c].remove(pseudo)
                    del self.acceptedPM[pseudo] # suppression de sa liste des clients ayant accepter une conversation privé
                    clientsocket.close() # fermeture du socket avec ce client
                    self.SendNotif(pseudo, "disconnected") # notification a tout les clients que "pseudo" est parti
                    break
                elif commande == "AFK": # le client veut passer en afk
                    if afk: #si il est deja afk 
                        reponse = "510 ERR_ALREADY_AFK"
                    else:
                        afk = True # mise a jour de sont status
                        self.SendNotif(pseudo, "is away from keyboard") # notification de sont nouveau status a tout les client connecté
                        reponse = "100 SUCCESS"
                elif commande == "BAK": # le client veut sortir de l'etat afk
                    if not afk:
                        reponse = "520 ERR_NOT_AFK"
                    else:
                        afk = False # mise a jour de sont status
                        self.SendNotif(pseudo, "is back at keybord") # notification de sont nouveau status a tout les client connecté
                        reponse = "100 SUCCESS"
                elif commande == "WHOSCO": # le cleint demande la liste des clients conncté
                    reponse = "101 SUCCESS " + " ".join(self.clients.keys()) #renvoie un SUCCESS suivi de la liste de tout les clients connecté separé par un espace 
                elif not afk: # si le cleint n'est pas AFK (toute les commande suivante necessite que le client soit actif)
                    if commande == "SEND": # le client veut envoyer un message sur le chat global
                        if arg: # si il y a des argument ( message pas vide)
                            for clientSock in self.clients.values(): # envoie du message a tout les clients connecté
                                try:
                                    clientSock.send(("SENDALL " + pseudo + " " + arg + "\n").encode())
                                except Exception: # si le send echoue, le client a surmement fermé le socket
                                    pass #la deconncetion du client et la fermeture du socket de cet utilisateur sera geré dans son thread au moment du recv
                            reponse = "100 SUCCESS"

                    elif commande == "ASKPM": # le cleint veut discuter avec le client ayant le pseudo contenue dans arg en privé
                        if arg in self.clients.keys(): # si le pseudo a contacté existe (est connecté)
                            if arg in self.acceptedPM[pseudo]: # si le cleint a deja accepté la demande, renvoie de accepté au demandeur
                                reponse = "110 ACCEPT " + arg
                            else:
                                socketPM = self.clients[arg]
                                socketPM.send(("ASKPM " + pseudo + "\n").encode()) # demande a "arg" si il accepte une conversation privé avec "pseudo"
                                self.askedPM[arg].append(pseudo)
                                # creation d'un nouveau thread pour ne pas bloqué le demandeur en attendant la reponse du receveur
                                # thread = threading.Thread(target = self.AskPm, args = (arg, pseudo, ip, clientsocket)) 
                                # thread.start()
                                continue
                        else: # le client demandé n'est pas cnnecté
                            reponse = "600 ERR_CLIENT_NOT_FOUND"

                    elif commande == "RENAME": # le client veut se renomé
                        if self.nameValid(arg): # le nouveau pseudo de satisfait pas les regles de la rfc
                            reponse = "400 ERR_INVALID_NICK_NAME"
                        elif arg in self.clients.keys(): # le pseudo est deja utilisé
                            reponse = "410 ERR_PSEUDO_ALREADY_USE"
                        else:
                            self.SendNotif(pseudo, "is now " + arg) # notification a tout les clients que "pseudo" est devenu "arg"
                            
                            # traitment du changement de pseudo pour les messages privés
                            self.acceptedPM[arg] = self.acceptedPM[pseudo] 
                            del self.acceptedPM[pseudo]
                            for c in self.acceptedPM[arg]:
                                self.acceptedPM[c].append(arg)
                                self.acceptedPM[c].remove(pseudo)

                            # changement du pseudo courant et dans le dictionnaire des conneté 
                            del(self.clients[pseudo])
                            pseudo = arg
                            self.clients[arg] = clientsocket
                            reponse = "100 SUCCESS"
                    elif commande == "PM": # le client envoie un message privé
                        if " " in arg:
                            name, contenu = arg.split(" ",1) # separation du destinataire et du message
                            print(f"namepm {name}")
                            print(self.clients.keys())
                            if name in self.clients.keys(): # si il existe
                                if name in self.acceptedPM[pseudo]: # et qu'il a accepté les massage privé de l'envoyeur
                                    self.clients[name].send(("SENDPM " + pseudo + " " + contenu + "\n").encode()) # transfert du message privé au destinataire
                                    self.logger.info("to " + ip + ": SENDPM " + pseudo + " " + contenu)
                                    reponse = "100 SUCCESS" # retour d'un success a l'envoyeur
                                else:
                                    reponse = "610 ERR_PM_NOT_ACCEPTED" #le destinataire n'as pas accepté les messages privé de l'envoyeur
                            else:
                                reponse = "600 ERR_CLIENT_NOT_FOUND"

                    elif commande == "SENDFILE":
                        if message.count(' ') == 2:
                            nameTo, fileInf = message.split(" ",1)
                            if nameTo in self.clients.keys():

                                socketPM = self.clients[nameTo]
                                socketPM.send(("SENDFILE " + pseudo + " " + fileInf + "\n").encode())
                                self.askedFile[nameTo].append(pseudo)
                                continue

                            else:
                                reponse = "600 ERR_CLIENT_NOT_FOUND"
                    else:
                        reponse = "200 ERR_INVALID_COMMAND"
                else:
                    reponse = "500 ERR_CLIENT_AFK"
            else:
                reponse = "310 ERR_NOT_CONNECTED"
            clientsocket.send((reponse + "\n").encode())
            self.logger.info("to " + ip + ": " + reponse)
        


def main():
    try:
        with open('conf.json') as json_data: # ouverture du ficher de config
            conf = json.load(json_data)
            # si une des valeur est manquante elle est remplacé par une valeur par defaut
            if not conf.get("ip"):
                conf["ip"] = ''
            if not conf.get("port"):
                conf["port"] = 8080
            serv = Serv(conf.get("ip"), conf.get("port"))
    except IOError:
        serv = Serv('', 8080)#si pas de fichier utilise '' et 8080 comme valeur par defaut

if __name__ == "__main__":
    main()